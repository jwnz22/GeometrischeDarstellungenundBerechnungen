# Thema Geometrische Darstellungen und Berechnungen

## Lukas Philippus; Julian Wenzel

## Funktionsbeschreibung
-	Kann Dreiecke, Vierecke und Kreise durch Eingabe von Punkten und/oder Größen zeichnen
-	Verschiedene Größen werden berechnet
-	Verschieden Linien können angezeigt werden (Höhenlinie, etc.)
-	Prüfung ob ein gültiges Objekt vorliegt
-	(mögliche Darstellung im 3-Dimenionalen + Berechnungen)


## Bezug zu Mathematik
Die Geometrie ist ein elementarer Bestandteil der Mathematik

## Zeitplan
1.	Grobkonzept Ende Oktober
2.	Feinkonzept Mitte November
3.	Bis 2. Woche Dezember: Programmierung Grundgerüst 
4.	Prototyp für Test Ende Dezember


## Aufgabenteilung
- Lukas Philippus: Programmierung, Präsentation
- Julian Wenzel: Benutzeroberfläche, Dokumentation, Präsentation


## Zusammenarbeit
-	Regelmäßige Treffen 1-mal wöchentlich
-	Bei Änderungen des Programms wird die Änderung per Mail verschickt

## Dokumentation
-	Basierend auf Javadoc und zusätzliche Dokumentation per Word-Dokument
-	Sprache: Deutsch



## Offene Punkte, Fragen
-	Entwicklungsumgebung Eclipse oder BlueJ verwenden?